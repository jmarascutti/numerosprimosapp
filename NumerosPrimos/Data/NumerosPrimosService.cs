﻿namespace NumerosPrimos.Data
{
    public class NumerosPrimosService
    {
        public bool VerificarNumero(string Numero)
        {
            bool _esPrimo = false;
            if (Int16.Parse(Numero) == 4)
            {
                _esPrimo = false;

            } else if (Int16.Parse(Numero) == 3)  {

                _esPrimo = true;

            }
            return _esPrimo;
           
        }
    }


}
