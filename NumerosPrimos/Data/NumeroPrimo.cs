﻿using System.ComponentModel.DataAnnotations;

namespace NumerosPrimos.Data
{
    public class NumeroPrimo
    {
      
        [Required(ErrorMessage = "Por favor, ingrese un número")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Por favor, ingrese sólo números")]
        [Range(2, Int32.MaxValue, ErrorMessage ="Por favor, ingrese un numero mayor o igual a 2")]     

        public string Number { get; set; }
    }
}
